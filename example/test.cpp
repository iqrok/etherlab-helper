#include "etherlab-helper.h"
#include <iostream>
#include <signal.h>
#include <unistd.h>

void handler(int code)
{
	EcatHelper::stop();
	printf("SIGNAL %d\n", code);
	exit(code);
}

int main(int argc, char* argv[])
{
	struct sigaction signalHandler;

	signalHandler.sa_handler = handler;
	sigemptyset(&signalHandler.sa_mask);
	signalHandler.sa_flags = 0;

	sigaction(SIGINT, &signalHandler, NULL);

	if (argc < 2) {
		fprintf(stderr, "Usage: %s '/path/to/slaves.json'\n", argv[0]);
		exit(0);
	}

	std::string json_path(argv[1]);

	EcatHelper::set_json_path(json_path);
	EcatHelper::set_frequency(5'000);
	EcatHelper::init();

	// SDO
	uint8_t flag = EcatHelper::SDO::read_u8(3, 0x8000, 0x02);
	printf("SDO flag: %x\n", flag);
	EcatHelper::SDO::write_u8(3, 0x8000, 0x02, 0x1);
	EcatHelper::SDO::write_u8(4, 0x8000, 0x02, 0x1);
	printf("SDO flag: %x\n", flag);
	flag = EcatHelper::SDO::read_u8(3, 0x8000, 0x02);
	printf("SDO flag: %x\n", flag);
	std::string test = EcatHelper::SDO::read_string(4, 0x1008, 0x00, 6);
	printf("SDO: %s\n", test.c_str());
	test = EcatHelper::SDO::read_string(30, 0x1008, 0x00, 6);
	printf("SDO: %s\n", test.c_str());

	usleep(100000);

	EcatHelper::start();
	printf("STARTED %d..........!!!\n", EcatHelper::operational_status());

	uint8_t orig = 0b10001;
	uint8_t byte = orig;
	uint32_t dus = 100'000;

	uint32_t values[] = { 0x200, 0x1fff, 0x2fff, 0x3fff, 0x4fff, 0x7fff };
	uint8_t _idx = 0;

	for (uint32_t another_value = 0; another_value < 10000;
		 another_value += 0x1, _idx ^= 1) {
		EcatHelper::Domain::write_bit(1, 0x7000, 0x01, byte & 0x01);
		EcatHelper::Domain::write_bit(1, 0x7010, 0x01, (byte & 0x02) >> 1);
		EcatHelper::Domain::write_bit(1, 0x7020, 0x01, (byte & 0x03) >> 2);
		EcatHelper::Domain::write_bit(1, 0x7030, 0x01, (byte & 0x04) >> 3);

		EcatHelper::Domain::write_u16(
			3, 0x7000, 0x01, values[another_value % 6]);
		EcatHelper::Domain::write_u16(3, 0x7010, 0x01, values[_idx]);

		EcatHelper::Domain::write_bit(6, 0x7000, 0x01, ~byte & 0x01);
		EcatHelper::Domain::write_bit(6, 0x7010, 0x01, ~(byte & 0x02) >> 1);
		EcatHelper::Domain::write_bit(6, 0x7020, 0x01, ~(byte & 0x04) >> 2);
		EcatHelper::Domain::write_bit(6, 0x7030, 0x01, ~(byte & 0x08) >> 3);
		EcatHelper::Domain::write_bit(6, 0x7040, 0x01, ~(byte & 0x10) >> 4);
		EcatHelper::Domain::write_bit(6, 0x7050, 0x01, ~(byte & 0x20) >> 5);
		EcatHelper::Domain::write_bit(6, 0x7060, 0x01, ~(byte & 0x40) >> 6);
		EcatHelper::Domain::write_bit(6, 0x7070, 0x01, ~(byte & 0x80) >> 7);

		EcatHelper::Domain::write_bit(9, 0x7000, 0x01, byte & 0x01);
		EcatHelper::Domain::write_bit(9, 0x7010, 0x01, (byte & 0x02) >> 1);
		EcatHelper::Domain::write_bit(9, 0x7020, 0x01, (byte & 0x04) >> 2);
		EcatHelper::Domain::write_bit(9, 0x7030, 0x01, (byte & 0x08) >> 3);
		EcatHelper::Domain::write_bit(9, 0x7040, 0x01, (byte & 0x10) >> 4);
		EcatHelper::Domain::write_bit(9, 0x7050, 0x01, (byte & 0x20) >> 5);
		EcatHelper::Domain::write_bit(9, 0x7060, 0x01, (byte & 0x40) >> 6);
		EcatHelper::Domain::write_bit(9, 0x7070, 0x01, (byte & 0x80) >> 7);

		EcatHelper::Domain::write_bit(21, 0x7000, 0x01, byte & 0x01);
		EcatHelper::Domain::write_bit(21, 0x7010, 0x01, (byte & 0x02) >> 1);
		EcatHelper::Domain::write_bit(21, 0x7020, 0x01, (byte & 0x04) >> 2);
		EcatHelper::Domain::write_bit(21, 0x7030, 0x01, (byte & 0x08) >> 3);
		EcatHelper::Domain::write_bit(21, 0x7040, 0x01, (byte & 0x10) >> 4);
		EcatHelper::Domain::write_bit(21, 0x7050, 0x01, (byte & 0x20) >> 5);
		EcatHelper::Domain::write_bit(21, 0x7060, 0x01, (byte & 0x40) >> 6);
		EcatHelper::Domain::write_bit(21, 0x7070, 0x01, (byte & 0x80) >> 7);

		uint8_t leftmost = (byte & 0x80) >> 7;
		byte <<= 1;

		if (leftmost)
			byte |= leftmost;
		else
			byte &= ~leftmost;

		if (byte == 0) {
			byte = orig;
		}

		printf("DO = %d\n", EcatHelper::Domain::read_i16(1, 0x7000, 0x01));
		printf("AO = %d\n", EcatHelper::Domain::read_i16(3, 0x7000, 0x01));

		usleep(750);

		printf("DI = %4x\n", EcatHelper::Domain::read_bit(2, 0x6000, 0x01));
		printf("DI = %4x\n", EcatHelper::Domain::read_bit(2, 0x6010, 0x01));
		printf("DI = %4x\n", EcatHelper::Domain::read_bit(2, 0x6020, 0x01));
		printf("DI = %4x\n", EcatHelper::Domain::read_bit(2, 0x6030, 0x01));
		printf("AI1 = %d\n", EcatHelper::Domain::read_i16(4, 0x6000, 0x11));
		printf("AI2 = %d\n\n", EcatHelper::Domain::read_i16(4, 0x6010, 0x011));
		printf("dus = %d\n\n", dus);
		//~ write(1, 0x7010, 0x01, values[_idx ^ 1]);
		//~ write(1, 0x7020, 0x01, another_value);
		//~ write(1, 0x7030, 0x01, 0x7000);

		//~ read(2, 0x6000, 0x01);

		//~ printf("%d\t%d\t%d\t%d\n", read(2, 0x6000, 0x01), read(2, 0x6010,
		// 0x01), read(2, 0x6020, 0x01), read(2, 0x6030, 0x01));

		//~ another_value %= 0x8000;
		usleep(dus);

		if (dus > 1'000) {
			dus -= 100;
		}
	}

	printf("DONE..........!!!\n");
	EcatHelper::stop();

	return 0;
}
