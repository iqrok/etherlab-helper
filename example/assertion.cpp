#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#define DOCTEST_CONFIG_SUPER_FAST_ASSERTS

#include "doctest/doctest.h"
#include <etherlab-helper.h>
#include <unistd.h>

bool is_ai_tolerated(int32_t value, int32_t target, uint16_t tolerance)
{
	int32_t diff = value - target;
	int32_t range = static_cast<int32_t>(tolerance);
	return (diff >= -range) && (diff <= range);
}

TEST_CASE("Initialization Test")
{
	constexpr int MAX_PATH = 0xff;
	char cwd[0xff];
	getcwd(cwd, MAX_PATH);

	std::string json_file = std::string(cwd) + std::string("/slaves.json");

	uint16_t frequency = 2000;
	uint32_t period_ns = 500'000;

	EcatHelper::set_json_path(json_file);
	EcatHelper::set_frequency(frequency);
	EcatHelper::init();

	REQUIRE_MESSAGE(EcatHelper::get_frequency() == frequency, "Check Frequecy");
	REQUIRE_MESSAGE(
		EcatHelper::get_period() == period_ns, "Check Period in ns");
}

TEST_CASE("SDO Operation Test")
{
	REQUIRE_MESSAGE(
		EcatHelper::SDO::read_string(3, 0x1008, 0x00, 6) == "EL4004",
		"SDO Read string Slave 3");

	REQUIRE_MESSAGE(
		EcatHelper::SDO::read_string(4, 0x1008, 0x00, 6) == "EL3104",
		"SDO Read string Slave 4");

	REQUIRE_MESSAGE(EcatHelper::SDO::read_string(30, 0x1008, 0x00, 6).empty(),
		"Wrong Attempt at reading string");

	REQUIRE_MESSAGE(EcatHelper::SDO::write_u8(3, 0x8000, 0x02, 1) == 0,
		"SDO Write Slave 3");

	REQUIRE_MESSAGE(
		EcatHelper::SDO::read_u8(3, 0x8000, 0x02) == 1, "SDO Read Slave 3");

	REQUIRE_MESSAGE(EcatHelper::SDO::write_u8(4, 0x8000, 0x02, 0) == 0,
		"SDO Write Slave 4");

	REQUIRE_MESSAGE(
		EcatHelper::SDO::read_u8(4, 0x8000, 0x02) == 0, "SDO Read Slave 4");

	REQUIRE_MESSAGE(EcatHelper::SDO::read_u16(4, 0x8000, 0x02) == 0,
		"Wrong size read attempt");
}

TEST_CASE("Domain Operation Test")
{
	int16_t ana_in;
	int16_t tolerance = 80;
	EcatHelper::ecat_size_io_al dmn_idx = 0;

	EcatHelper::start();
	REQUIRE(EcatHelper::operational_status());

	REQUIRE_MESSAGE(EcatHelper::Domain::write_u16(3, 0x7000, 0x01, 0xffff) == 0,
		"Domain Write Slave 3");
	usleep(10'000);
	REQUIRE_MESSAGE(EcatHelper::Domain::read_u16(3, 0x7000, 0x01) == 0xffff,
		"Domain Read Slave 3");

	ana_in = EcatHelper::Domain::read_i16(4, 0x6000, 0x11);
	CHECK_MESSAGE(is_ai_tolerated(ana_in, 0x7fff, tolerance), "AnaIn ", ana_in);

	REQUIRE_MESSAGE(EcatHelper::Domain::write_u16(3, 0x7000, 0x01, 0x00) == 0,
		"Domain Write Slave 3");
	usleep(15'000);
	REQUIRE_MESSAGE(EcatHelper::Domain::read_u16(3, 0x7000, 0x01) == 0x00,
		"Domain Read Slave 3");

	ana_in = EcatHelper::Domain::read_i16(4, 0x6000, 0x11);
	CHECK_MESSAGE(is_ai_tolerated(ana_in, 0, tolerance), "AnaIn ", ana_in);

	REQUIRE_MESSAGE(
		EcatHelper::get_domain_index(4, 0x7000, 0x11, &dmn_idx) != 0,
		"Domain Index ", dmn_idx);

	REQUIRE_MESSAGE(
		EcatHelper::get_domain_index(4, 0x6000, 0x11, &dmn_idx) == 0,
		"Domain Index ", dmn_idx);

	EcatHelper::stop();
	REQUIRE_FALSE(EcatHelper::operational_status());
}

TEST_CASE("Test Pointer to data")
{
	EcatHelper::start();

	REQUIRE(EcatHelper::operational_status());
	REQUIRE_MESSAGE((EcatHelper::application_layer_states() & EC_AL_STATE_OP),
		"Application Layer States");

	EcatHelper::ecat_entries_al* process_data;
	EcatHelper::ecat_domain_map_al* domain_map;

	EcatHelper::ecat_pos_al s_position = 1;
	EcatHelper::ecat_index_al s_index = 0x7000;
	EcatHelper::ecat_sub_al s_subindex = 0x01;

	uint16_t write_val = 0x1;
	uint32_t key = (s_position << 24) | (s_index << 8) | (s_subindex << 0);

	EcatHelper::attach_process_data(&process_data);
	EcatHelper::attach_mapped_domain(&domain_map);

	REQUIRE(EcatHelper::Domain::write_bit(
				s_position, s_index, s_subindex, write_val)
		== 0);

	usleep(10'000);

	REQUIRE(EcatHelper::Domain::read_bit(s_position, s_index, s_subindex)
		== write_val);
	EcatHelper::ecat_size_io_al domain_idx = domain_map->at(key);
	EcatHelper::ecat_value_al read_val = process_data->at(domain_idx).value;

	REQUIRE(read_val.u8 == write_val);

	s_position = 3;
	s_index = 0x7000;
	s_subindex = 0x01;

	write_val = 0x7fff;
	key = (s_position << 24) | (s_index << 8) | (s_subindex << 0);

	REQUIRE(EcatHelper::Domain::write_u16(
				s_position, s_index, s_subindex, write_val)
		== 0);

	usleep(10'000);

	REQUIRE(EcatHelper::Domain::read_u16(s_position, s_index, s_subindex)
		== write_val);
	domain_idx = domain_map->at(key);
	read_val = process_data->at(domain_idx).value;

	REQUIRE(read_val.u16 == write_val);

	EcatHelper::stop();
	REQUIRE_FALSE(EcatHelper::operational_status());
}

TEST_CASE("Restart and Loop")
{
	EcatHelper::start();

	REQUIRE(EcatHelper::operational_status());
	REQUIRE_MESSAGE((EcatHelper::application_layer_states() & EC_AL_STATE_OP),
		"Application Layer States");

	int16_t ana_in;
	int16_t ana_out = 0x100;
	int16_t tolerance = 0x37;
	int32_t target;
	int16_t max = 20;
	for (int16_t i = 0; i <= max; i++, ana_out += 200) {
		uint8_t val1 = i % 2;
		uint8_t val2 = val1 ^ 1;

		REQUIRE_MESSAGE(
			EcatHelper::Domain::write_u16(3, 0x7000, 0x01, ana_out) == 0,
			"AnaOut ", ana_out);

		REQUIRE(EcatHelper::Domain::write_bit(1, 0x7010, 0x01, val1) == 0);
		REQUIRE(EcatHelper::Domain::write_bit(21, 0x7000, 0x01, val2) == 0);

		usleep(15'000);

		ana_in = EcatHelper::Domain::read_i16(4, 0x6000, 0x11);
		target = ana_out >> 1;
		CHECK_MESSAGE(is_ai_tolerated(ana_in, target, tolerance), target, ": ",
			(target - tolerance), " <= ", ana_in, " <= ", (target + tolerance));

		REQUIRE(EcatHelper::Domain::read_bit(2, 0x6010, 0x01) == val1);
		REQUIRE(EcatHelper::Domain::read_bit(22, 0x6000, 0x01) == val2);

		usleep(2'000);
	}

	EcatHelper::stop();
	REQUIRE_FALSE(EcatHelper::operational_status());
	REQUIRE_MESSAGE(
		(EcatHelper::application_layer_states() & EC_AL_STATE_PREOP),
		"Application Layer States");
}
